from typing import Dict, List

from pydantic import BaseModel

from midi.midi_const import DEFAULT_SF2
from model.composition import Composition
from model.sequence import Sequence
from model.track import TrackVersion, Track


class Project(BaseModel):
    name: str
    compositions: List[Composition]


def sample_project() -> Project:
    track_version = TrackVersion(channel=100, version_name='Bass 0', num_of_bars=16, sf_name=DEFAULT_SF2,
                                 sequence=Sequence(num_of_bars=16))
    track_version1 = TrackVersion(channel=101, version_name='Version 1', num_of_bars=16, sf_name=DEFAULT_SF2,
                                  sequence=Sequence(num_of_bars=16))
    track_version2 = TrackVersion(channel=102, version_name='Version 2', num_of_bars=16, sf_name=DEFAULT_SF2,
                                  sequence=Sequence(num_of_bars=16))
    return Project(name="Sample project",
                   compositions=[Composition(name="Default", bpm=90,
                                             tracks=[Track(name="Bass",
                                                           current_version="Default",
                                                           versions=[track_version]),
                                                     Track(name="Guitar",
                                                           current_version="Default",
                                                           versions=[track_version1])
                                                     ]
                                             ),
                                 Composition(name="Second", bpm=90,
                                             tracks=[Track(name="Second track",
                                                           current_version="Version 0",
                                                           versions=[track_version]),
                                                     Track(name="Third track",
                                                           current_version="Version 2",
                                                           versions=[track_version1, track_version2])
                                                     ]
                                             )
                                 ])
