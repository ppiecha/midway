import copy
from typing import List, Union, Optional

from pydantic import BaseModel, PositiveInt, NonNegativeInt, NonNegativeFloat

from model.note import GenericEvent, GenericNote
from lib4py import logger as lg
import logging

logger = lg.get_console_logger(name=__name__, log_level=logging.DEBUG)

_notes = List[Union[GenericEvent, type(None)]]


class Bar(BaseModel):
    numerator: PositiveInt = 4
    denominator: PositiveInt = 4
    bar_num: NonNegativeInt
    length: NonNegativeFloat = float(numerator) * (1.0 / float(denominator))
    bar: List[GenericEvent] = []

    def clear(self):
        self.bar.clear()

    def _add_note(self, note: GenericEvent) -> None:
        if note.beat >= self.length:
            raise ValueError(f"Item outside of bar range {note.beat}")
        self.bar.append(note)
        self.bar.sort(key=lambda x: x.beat)

    def note_index(self, note: GenericEvent) -> int:
        """Index of note in bar list"""
        return self.bar.index(note)

    def remove_note(self, note: GenericEvent) -> None:
        """Removes note from bar"""
        self.bar.remove(note)

    # def note_exists(self, note: GenericNote) -> bool:
    #     try:
    #         if isinstance(note, Note):
    #             index =
    #         return True
    #     except ValueError:
    #         return False

    # def _delete_note(self, note: GenericNote) -> None:
    #     beat_notes = [item for item in self.bar if isinstance(item, type(note)) and item.beat == note.beat]
    #     if not beat_notes:
    #         raise ValueError(f"no {type(note)} on beat {note.beat}")
    #     else:
    #         if isinstance(note, Note):
    #             found = [item for item in beat_notes if int(item) == int(note)]
    #             if len(found) != 1:
    #                 raise ValueError(f"Found none or more than one note {repr(note)} on beat {note.beat}")
    #             else:
    #                 self.bar.remove(found[0])
    #                 logger.debug(f"note deleted {note}")
    #         else:
    #             if len(beat_notes) > 1:
    #                 raise ValueError(f"Found none or more than one note {repr(note)} on beat {note.beat}")
    #             else:
    #                 self.bar.remove(beat_notes[0])

    def delete_note(self, note: GenericEvent) -> None:
        # print(f"Note Index {self.note_index(note)}")
        self.remove_note(note=note)
        # self._delete_note(note)

    def delete_notes(self, notes: Optional[List[GenericNote]]) -> None:
        for note in notes:
            self.delete_note(note=note)

    def _add(self, this, other):
        if issubclass(type(other), GenericEvent):
            this._add_note(other)
        elif isinstance(other, Bar):
            for note in other:
                this._add_note(note=note)
        else:
            raise ValueError(f"Unsupported type {type(other)}")
        return this

    def __add__(self, other):
        bar = copy.deepcopy(self)
        return self._add(this=bar, other=other)

    def __iadd__(self, other):
        return self._add(this=self, other=other)

    def __getitem__(self, index):
        """Enable the  '[]' notation on Bars to get the item at the index."""
        return self.bar[index]

    def notes(self):
        return (note for note in self.bar)

    def __repr__(self):
        """Enable str() and repr() for Bars."""
        return str(self.bar)

    def __len__(self):
        """Enable the len() method for Bars."""
        return len(self.bar)
