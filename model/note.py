from typing import Optional, NewType

from midi.midi_const import DEFAULT_VELOCITY, CONTROL_NAME
from mingus.containers import Note as MingusNote
from model.types import Int, Float
from pydantic import BaseModel, conint, confloat, NonNegativeInt

MidiValue = NewType('MidiValue', conint(ge=0, le=127))
ChannelValue = NewType('ChannelValue', conint(ge=0, le=255))


class Preset(BaseModel):
    sfid: NonNegativeInt
    bank: MidiValue
    patch: MidiValue


DEFAULT_PRESET = Preset(sfid=0, bank=0, patch=0)


class GenericEvent(BaseModel):
    channel: ChannelValue
    beat: confloat(ge=0)


class ProgramEvent(GenericEvent):
    preset: Preset

    def __repr__(self):
        return f"(beat: {self.beat} s: {self.preset.sfid} b: {self.preset.bank} p: {self.preset.patch})"


class ControlEvent(GenericEvent):
    control: Optional[conint(ge=0, le=127)]
    value: Optional[conint(ge=0, le=127)]

    def __repr__(self):
        return f"(beat: {self.beat} " \
               f"c: {CONTROL_NAME[self.control] if self.control in CONTROL_NAME.values() else None} " \
               f"v: {self.value})"


class GenericNote(GenericEvent):
    pitch: MidiValue
    unit: confloat(ge=0)
    velocity: MidiValue = DEFAULT_VELOCITY


class Note(MingusNote, GenericNote):
    def __init__(self, pitch: Int = None, channel: Int = None, beat: Float = None, unit: Float = None,
                 velocity: Int = DEFAULT_VELOCITY):
        super().__init__(channel=channel, velocity=velocity)
        self.from_int(pitch)
        self.unit: Float = unit
        self.beat = beat

    def get_note(self) -> GenericNote:
        return GenericNote(channel=self.channel, beat=self.beat, pitch=int(self), unit=self.unit)

    def __repr__(self):
        return f"(n: {super().__repr__()} b: {self.beat} u: {self.unit})"
