from typing import Dict

from PySide6.QtCore import QSize
from PySide6.QtGui import Qt, QIcon
from PySide6.QtWidgets import QWidget, QTabWidget, QSplitter, QStackedWidget, QBoxLayout
from pubsub import pub

from constants import NEW_TRACK, EDIT_TRACK, DELETE_TRACK
from gui.sequence_grid import CustomLoopGrid, CompositionLoopGrid
from gui.track_list import TrackList
from gui.widgets import Box
from midi.synth import FS
from model.composition import Composition
from model.project import Project
from model.track import Track
import resources


class CompositionTab(QWidget):
    def __init__(self, mf, parent, project: Project, synth: FS):
        super().__init__(parent=parent)
        self.project = project
        self.map: Dict[str, TrackList] = {}
        self.tab_box = QTabWidget(self)
        for composition in self.project.compositions:
            tracks_splitter = QSplitter(Qt.Horizontal)
            tracks_stack = QStackedWidget(self)
            self.map[composition.name] = TrackList(mf=mf, parent=tracks_splitter, stack=tracks_stack,
                                                   composition=composition)
            tracks_splitter.addWidget(self.map[composition.name])
            tracks_splitter.addWidget(tracks_stack)
            vert_splitter = QSplitter(Qt.Vertical)
            self.seq_box = SequencerBox(parent=vert_splitter, mf=mf, composition=composition)
            vert_splitter.addWidget(tracks_splitter)
            vert_splitter.addWidget(self.seq_box)
            self.tab_box.addTab(vert_splitter, QIcon(":/icons/composition.png"), composition.name)
        self.main_box = Box(direction=QBoxLayout.TopToBottom)
        self.main_box.addWidget(self.tab_box)
        self.setLayout(self.main_box)

    @property
    def current_track_list(self) -> TrackList:
        vert_splitter: QSplitter = self.tab_box.currentWidget()
        tracks_splitter: QSplitter = vert_splitter.widget(0)
        track_list: TrackList = tracks_splitter.widget(0)
        if not track_list:
            raise ValueError(f'Cannot determine track list in current composition')
        return track_list

    def init_fonts(self):
        for composition in self:
            for track in self[composition]:
                for track_version in self[composition][track].version_tab:
                    self[composition][track].version_tab[track_version].track.init_fonts()

    def set_keyboard_position(self):
        for composition in self:
            for track in self[composition]:
                for track_version in self[composition][track].version_tab:
                    self[composition][track].version_tab[track_version].track.set_keyboard_position()

    @property
    def name(self) -> str:
        return self.project.name

    @name.setter
    def name(self, value: str) -> None:
        self.project.name = value

    def __iter__(self):
        return iter(self.map)

    def __getitem__(self, composition: str) -> TrackList:
        track_list = self.map.get(composition)
        if track_list is None:
            raise IndexError
        else:
            return track_list

    def __len__(self):
        return len(self.map)


class SequencerBox(QWidget):
    def __init__(self, mf, parent, composition: Composition):
        super().__init__(parent=parent)
        self.splitter = QSplitter(Qt.Horizontal)
        self.custom_loop_box = CustomLoopBox(parent=self.splitter, mf=mf, composition=composition)
        self.splitter.addWidget(self.custom_loop_box)
        self.composition_loop_box = CompositionLoopBox(parent=self.splitter, mf=mf, composition=composition)
        self.splitter.addWidget(self.composition_loop_box)
        self.main_box = Box(direction=QBoxLayout.TopToBottom)
        # self.main_box.setContentsMargins(5, 5, 5, 5)
        self.main_box.setSpacing(5)
        self.main_box.addWidget(self.splitter)
        self.setLayout(self.main_box)
        self.resize(QSize(self.width(), 400))
        self.register_listeners()

    def register_listeners(self):
        if not pub.subscribe(self.new_track, NEW_TRACK):
            raise Exception(f"Cannot register listener {NEW_TRACK}")
        if not pub.subscribe(self.rename_track, EDIT_TRACK):
            raise Exception(f"Cannot register listener {EDIT_TRACK}")
        if not pub.subscribe(self.delete_track, DELETE_TRACK):
            raise Exception(f"Cannot register listener {DELETE_TRACK}")

    def new_track(self, composition: Composition, track: Track):
        self.custom_loop_box.custom_loop_grid.load_loops()
        self.composition_loop_box.composition_loop_grid.load_loops()

    def rename_track(self, composition: Composition, track: Track, new_name: str):
        self.new_track(composition=composition, track=track)

    def delete_track(self, composition: Composition, track: Track):
        self.new_track(composition=composition, track=track)


class CustomLoopBox(QWidget):
    def __init__(self, mf, parent, composition: Composition):
        super().__init__(parent=parent)
        self.main_box = Box(direction=QBoxLayout.LeftToRight)
        self.custom_loop_grid = CustomLoopGrid(parent=self, mf=mf, composition=composition)
        self.main_box.addWidget(self.custom_loop_grid, stretch=1)
        self.setLayout(self.main_box)


class CompositionLoopBox(QWidget):
    def __init__(self, mf, parent, composition: Composition):
        super().__init__(parent=parent)
        self.main_box = Box(direction=QBoxLayout.TopToBottom)
        self.composition_loop_grid = CompositionLoopGrid(parent=self, mf=mf, composition=composition)
        self.main_box.addWidget(self.composition_loop_grid)
        self.setLayout(self.main_box)
