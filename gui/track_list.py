"""
Module contains track item and list of track items definition.
It controls adding and deleting tracks in composition
"""

from __future__ import annotations
import sys
from typing import Optional, Dict

from PySide6.QtCore import QSize
from PySide6.QtGui import QIcon, QPixmap, Qt
from PySide6.QtWidgets import QWidget, QBoxLayout, QComboBox, QListWidget, QListWidgetItem, QApplication, \
    QSpinBox, QLabel, QStackedWidget, QFrame, QToolBar
from pubsub import pub

from constants import DARK_PALETTE, NEW_TRACK, EDIT_TRACK, DELETE_TRACK
from gui.dialogs.generic_config import GenericConfig, GenericConfigMode
from gui.track_tab import TrackVersionTab
from gui.widgets import Box
from model.composition import Composition
from model.track import Track, TrackVersion
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from gui.main_frame import MainFrame

import resources


class TrackListItem(QWidget):
    def __init__(self, mf: MainFrame, parent, track: Track, list_item: QListWidgetItem, composition: Composition):
        super().__init__(parent=parent)
        self.track = track
        self.list_item = list_item
        self.version_tab = TrackVersionTab(mf=mf, list_item=self, track=track, synth=mf.synth, composition=composition)
        self.icon = QLabel()
        self.icon.setPixmap(QPixmap(":/icons/track_item.png"))
        self.name = QLabel(track.name)
        self.main_box = Box(direction=QBoxLayout.LeftToRight)
        self.main_box.setContentsMargins(5, 0, 0, 0)
        self.frame = QFrame(self)
        self.frame.setFrameStyle(QFrame.StyledPanel | QFrame.Raised)
        self.frame.setLayout(self.main_box)
        self.frame_box = Box(direction=QBoxLayout.LeftToRight)
        self.frame_box.addWidget(self.frame)
        self.main_box.addWidget(self.icon)
        self.main_box.setSpacing(5)
        self.main_box.addWidget(self.name)
        self.main_box.addStretch()

        self.setLayout(self.frame_box)

        # self.update_version_list(current_version=current_version)

    @property
    def current_track_version(self) -> TrackVersion:
        return self.version_tab.current_track_version

    @current_track_version.setter
    def current_track_version(self, track_version: TrackVersion) -> None:
        self.version_tab.current_track_version = track_version

    # def on_current_changed(self, index):
    #     self.version_tab.select_current_version(current_version=self.w_version.itemText(index))
    #
    # def update_version_list(self, current_version: str):
    #     self.w_version.clear()
    #     self.w_version.addItems(self.version_tab.versions)
    #     self.w_version.setCurrentText(current_version)
    #
    # @property
    # def current_version(self) -> str:
    #     return self.w_version.currentText()
    #
    # @current_version.setter
    # def current_version(self, value: str) -> None:
    #     if self.w_version.currentText() != value:
    #         self.w_version.setCurrentText(value)
    #     self.version_tab.current_version = value


class TrackList(QListWidget):
    def __init__(self, mf: MainFrame, parent, stack: QStackedWidget, composition: Composition):
        super().__init__(parent=parent)
        self.mf = mf
        self.composition = composition
        self.list = QListWidget(self)
        self.map: Dict[str, TrackListItem] = {}
        self.stack = stack
        for track in composition.tracks:
            self._new_track(track=track)
        self.select_first_item()
        self.main_box = Box(direction=QBoxLayout.TopToBottom)
        self.toolbar = TrackListToolbar(mf=mf, track_list=self)
        self.main_box.addWidget(self.toolbar)
        self.main_box.addWidget(self.list)
        self.setLayout(self.main_box)

        self.list.currentRowChanged.connect(self.display)
        self.list.itemActivated.connect(self.edit_track)

        self.register_listeners()

    @property
    def current_track_list_item(self) -> TrackListItem:
        current_item = self.list.currentItem()
        if not current_item:
            raise ValueError(f'Cannot determine current item in track list in composition {self.composition}')
        return self.list.itemWidget(current_item)

    def select_first_item(self):
        if self.list.count():
            self.list.setCurrentRow(0)

    def _new_track(self, track: Track):
        widget_item = QListWidgetItem(self.list)
        self.map[track.name] = TrackListItem(mf=self.mf, parent=self, track=track,
                                             list_item=widget_item,
                                             composition=self.composition)
        self.stack.addWidget(self.map[track.name].version_tab)
        widget_item.setSizeHint(self.map[track.name].sizeHint())
        self.list.addItem(widget_item)
        self.list.setItemWidget(widget_item, self.map[track.name])

    def edit_track(self, item: QListWidgetItem):
        track_list_item: TrackListItem = self.list.itemWidget(item)
        config = GenericConfig(mf=self.mf, mode=GenericConfigMode.edit, composition=self.composition,
                               track=track_list_item.track)
        self.mf.show_config_dlg(config=config)

    def _delete_track(self, track: Track):
        if not self.composition.track_name_exists(track_name=track.name):
            raise ValueError(f'Track with name {track.name} does not exist in composition {self.composition.name}')
        else:
            widget: TrackListItem = self.map.pop(track.name)
            self.list.removeItemWidget(widget.list_item)
            self.stack.removeWidget(widget)
            widget.deleteLater()

    def register_listeners(self):
        if not pub.subscribe(self.new_track, NEW_TRACK):
            raise Exception(f"Cannot register listener {NEW_TRACK}")
        if not pub.subscribe(self.rename_track, EDIT_TRACK):
            raise Exception(f"Cannot register listener {EDIT_TRACK}")
        if not pub.subscribe(self.delete_track, DELETE_TRACK):
            raise Exception(f"Cannot register listener {DELETE_TRACK}")

    def unregister_listener(self, topic):
        pass

    def new_track(self, composition: Composition, track: Track):
        self._new_track(track=track)

    def rename_track(self, composition: Composition, track: Track, new_name: str):
        pass

    def delete_track(self, composition: Composition, track: Track):
        if self.composition == composition:
            self._delete_track(track=track)

    def display(self, current: int):
        self.stack.setCurrentIndex(current)

    def __iter__(self):
        return iter(self.map)

    def __getitem__(self, track_name) -> TrackListItem:
        track_list_item = self.map[track_name]
        if track_list_item is None:
            raise IndexError
        else:
            return track_list_item

    def __len__(self):
        return len(self.map)


class TrackListToolbar(QToolBar):
    def __init__(self, mf: MainFrame, track_list: TrackList):
        super().__init__("Track list", track_list)
        self.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.setIconSize(QSize(16, 16))
        self.addAction(mf.menu.actions[NEW_TRACK])


if __name__ == '__main__':
    app = QApplication([])
    app.setStyle('Fusion')  # Style needed for palette to work
    app.setPalette(DARK_PALETTE)
    window = TrackList(None)
    window.setGeometry(100, 100, 300, 300)
    window.show()
    sys.exit(app.exec_())
