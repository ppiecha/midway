from __future__ import annotations

import logging
import sys
from typing import Tuple

from PySide6.QtCore import Qt
from PySide6.QtGui import QKeySequence
from PySide6.QtWidgets import QWidget, QApplication, QBoxLayout

from constants import DARK_PALETTE, SINGLE_TRACK
from gui.menu import Action
from gui.editor.grid import GridView
from gui.editor.keyboard import KeyboardView
from gui.editor.ruler import RulerView, HeaderView
from gui.widgets import Box
from lib4py import logger as lg
from midi.midi_const import DEFAULT_SF2
from midi.synth import FS
from model.types import Bpm
from model.composition import Composition
from model.sequence import Sequence
from model.track import TrackVersion, Track
from model.types import LoopType

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from gui.main_frame import MainFrame

logger = lg.get_console_logger(name=__name__, log_level=logging.DEBUG)


class PianoRoll(QWidget):
    def __init__(self, mf, parent, track_version: TrackVersion, synth: FS, composition: Composition, track: Track):
        super().__init__(parent=parent)
        self.mf = mf
        self.composition = composition
        self.track = track
        self.track_version = track_version
        self.synth = synth
        self.setAutoFillBackground(True)
        self.grid_view = GridView(num_of_bars=self.num_of_bars)
        self.grid_view.verticalScrollBar().valueChanged.connect(self.on_change_ver)
        self.grid_view.horizontalScrollBar().valueChanged.connect(self.on_change_hor)
        self.keyboard = KeyboardView(channel=self.channel, callback=self.grid_view.mark, synth=synth)
        self.keyboard.verticalScrollBar().valueChanged.connect(self.on_change_ver)
        self.keyboard.horizontalScrollBar().valueChanged.connect(self.on_change_hor)
        self.grid_view.grid_scene.keyboard_view = self.keyboard
        self.ruler_view = RulerView(num_of_bars=self.num_of_bars, grid_view=self.grid_view)
        self.header_view = HeaderView()
        self.box_piano = Box(direction=QBoxLayout.LeftToRight)
        self.box_piano.addWidget(self.keyboard)
        self.box_piano.addWidget(self.grid_view)
        self.box_ruler = Box(direction=QBoxLayout.LeftToRight)
        self.box_ruler.addWidget(self.header_view)
        self.box_ruler.addWidget(self.ruler_view)
        self.box_main = Box(direction=QBoxLayout.TopToBottom)
        self.box_main.addLayout(self.box_ruler)
        self.box_main.addLayout(self.box_piano)

        self.ac_select_all = Action(mf=self.mf, caption="Select all", shortcut=QKeySequence.SelectAll,
                                    slot=self.select_all)
        self.ac_delete = Action(mf=self.mf, caption="Delete selected", shortcut=QKeySequence.Delete,
                                slot=self.delete_selected)
        self.ac_invert_sel = Action(mf=self.mf, caption="Invert selection", shortcut=QKeySequence(Qt.CTRL | Qt.Key_I),
                                    slot=self.invert_selection)
        self.ac_copy_sel = Action(mf=self.mf, caption="Copy selection", shortcut=QKeySequence.Copy,
                                  slot=self.copy_selection)
        self.ac_play = Action(mf=self.mf, caption="Play pianoroll sequence",
                              shortcut=QKeySequence(Qt.Key_Enter),
                              slot=self.play)
        self.ac_escape = Action(mf=self.mf, caption="Escape", shortcut=QKeySequence.Cancel, slot=self.escape)

        self.setLayout(self.box_main)
        self.sequence = self.track_version.sequence

    def play(self, mf: MainFrame):
        loop = self.composition.loops[LoopType.custom].get_loop_by_name(loop_name=SINGLE_TRACK)
        loop.set_single_track_version(track=self.track, track_version=self.track_version)
        self.synth.play_composition(self.composition, grid_type=LoopType.custom, loop_name=SINGLE_TRACK)

    def select_all(self, mf: MainFrame):
        self.grid_view.grid_scene.select_all()

    def delete_selected(self, mf: MainFrame):
        self.grid_view.grid_scene.delete_nodes(meta_notes=self.grid_view.grid_scene.selected_notes, hard_delete=True)

    def invert_selection(self, mf: MainFrame):
        self.grid_view.grid_scene.invert_selection()

    def copy_selection(self, mf: MainFrame):
        self.grid_view.grid_scene.copy_selection()

    def escape(self, mf: MainFrame):
        self.grid_view.grid_scene.escape()

    def undo(self, mf: MainFrame):
        pass

    @property
    def bpm(self) -> Bpm:
        return self.composition.bpm

    @property
    def channel(self) -> int:
        return self.track_version.channel

    @channel.setter
    def channel(self, value: int) -> None:
        self.track_version.channel = value

    @property
    def sequence(self):
        return self.track_version.sequence

    @sequence.setter
    def sequence(self, value: Sequence) -> None:
        self.track_version.sequence = value
        self.ruler_view.ruler_scene.sequence = value
        self.grid_view.grid_scene.sequence = value

    @property
    def num_of_bars(self) -> int:
        return self.track_version.num_of_bars

    @num_of_bars.setter
    def num_of_bars(self, value) -> None:
        self.track_version.num_of_bars = value
        self.ruler_view.num_of_bars = value
        self.grid_view.num_of_bars = value
        self.sequence.set_num_of_bars(value=value - 1)
        self.grid_view.horizontalScrollBar().setValue(1)
        self.grid_view.horizontalScrollBar().setValue(0)

    @property
    def sf_name(self) -> str:
        preset = self.synth.get_current_preset(channel=self.track_version.channel)
        return self.synth.sf_name(sfid=preset.sfid)

    @sf_name.setter
    def sf_name(self, value: str):
        self.synth.sfont_select(self.track_version.channel, self.synth.sf_map[value])

    @property
    def bank_patch(self):
        preset = self.synth.get_current_preset(channel=self.track_version.channel)
        return preset.bank, preset.patch

    @bank_patch.setter
    def bank_patch(self, value: Tuple[int, int]):
        bank, patch = value
        self.synth.program_select(self.track_version.channel, self.synth.sf_map[self.sf_name], bank, patch)

    def on_change_ver(self, value: int):
        self.keyboard.verticalScrollBar().setValue(value)
        self.grid_view.verticalScrollBar().setValue(value)

    def on_change_hor(self, value: int):
        self.ruler_view.horizontalScrollBar().setValue(value)
        self.grid_view.horizontalScrollBar().setValue(value)
