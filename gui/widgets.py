import logging
import threading
from pathlib import Path
from typing import Optional

from PySide6.QtCore import Qt
from PySide6.QtGui import QPainter
from PySide6.QtWidgets import QWidget, QBoxLayout, QGraphicsView, QFrame, QCheckBox, QComboBox

from lib4py import logger as lg
from midi.midi_const import CHANNELS
from midi.synth import FS
from model.composition import Composition
from model.note import Preset
from model.track import TrackLoopItem, Loop
from model.types import LoopType

logger = lg.get_console_logger(name=__name__, log_level=logging.DEBUG)


class SafeThread(threading.Thread):
    """ShellThread should always e used in preference to threading.Thread.
    The interface provided by ShellThread is identical to that of threading.Thread,
    however, if an exception occurs in the thread the error will be logged
    to the user rather than printed to stderr.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._real_run = self.run
        self.run = self._wrap_run

    def _wrap_run(self):
        try:
            logger.debug(f"Thread started")
            self._real_run()
            logger.debug(f"Thread stopped")
        except Exception as e:
            logger.error(str(e))


class Box(QBoxLayout):
    def __init__(self, direction):
        super().__init__(direction)
        self.setContentsMargins(0, 0, 0, 0)
        # self.setMargin(0)
        self.setSpacing(0)


class GraphicsView(QGraphicsView):
    def __init__(self, show_scrollbars: bool = False):
        super().__init__()
        self.setMouseTracking(True)
        self.setContentsMargins(0, 0, 0, 0)
        self.setViewportMargins(0, 0, 0, 0)
        self.setContentsMargins(0, 0, 0, 0)
        self.setFrameShape(QFrame.NoFrame)
        self.setRenderHint(QPainter.Antialiasing)
        self.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        self.horizontalScrollBar().setContentsMargins(0, 0, 0, 0)
        self.verticalScrollBar().setContentsMargins(0, 0, 0, 0)
        if not show_scrollbars:
            self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)


class TrackVersionBox(QWidget):
    def __init__(self, parent, composition: Composition, loop: Loop, loop_item: Optional[TrackLoopItem],
                 show_check_box: bool, show_combo: bool):
        super().__init__(parent)
        self.composition = composition
        self.loop = loop
        self.loop_item = loop_item
        self.is_loop_selector = not show_combo
        self.enabled = QCheckBox(self)
        self.enabled.setVisible(show_check_box)
        self.version = QComboBox(self)
        self.version.setVisible(show_combo)
        self.main_box = Box(direction=QBoxLayout.LeftToRight)
        self.main_box.setContentsMargins(5, 0, 0, 0)
        self.main_box.setSpacing(5)
        if not show_combo:
            self.main_box.setAlignment(Qt.AlignCenter)
        else:
            self.main_box.setAlignment(Qt.AlignLeft)
        self.main_box.addWidget(self.enabled)
        self.main_box.addWidget(self.version)
        self.setLayout(self.main_box)

        if show_combo:
            self.reload_versions()

        self.enabled.stateChanged.connect(self.on_enable_changed)

    def reload_versions(self, current_version: str = None):
        self.version.clear()
        self.version.addItems([version.version_name for version in self.loop_item.loop_track.versions])
        if current_version:
            self.version.setCurrentText(current_version)

    def on_enable_changed(self):
        if self.is_loop_selector:
            self.composition.get_loops(loop_type=LoopType.custom).set_checked_loop(self.loop)
        else:
            self.loop_item.loop_track_enabled = self.enabled.isChecked()


class ChannelBox(QComboBox):
    def __init__(self, default_channel: int = None):
        super().__init__()
        for channel in CHANNELS:
            if channel == 9:
                self.addItem(f'Drums (channel {channel})')
            else:
                self.addItem(f'Channel {str(channel)}', channel)
        if default_channel:
            self.setCurrentIndex(default_channel)

    def get_channel(self):
        return self.currentIndex()


class FontBox(QComboBox):
    def __init__(self, synth: FS):
        super().__init__()
        self.synth = synth

    def populate_font_combo(self):
        self.setEditable(False)
        self.setDuplicatesEnabled(False)
        self.clear()
        for font in self.synth.sf_map.keys():
            self.addItem(Path(font).name, font)


class PresetBox(QComboBox):
    def __init__(self, synth: FS):
        super().__init__()
        self.synth = synth

    def populate_preset_combo(self, sfid: int):
        self.clear()
        for bank in self.synth.preset_map[sfid]:
            for patch in self.synth.preset_map[sfid][bank]:
                preset = Preset(sfid=sfid, bank=bank, patch=patch)
                preset_map = self.synth.preset_map
                if sfid in preset_map and bank in preset_map[sfid] and patch in preset_map[sfid][bank]:
                    preset_name = preset_map[sfid][bank][patch]
                    self.addItem(f"{bank}:{patch} {preset_name}", preset)
