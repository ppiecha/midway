from __future__ import annotations

from enum import Enum, auto, Flag
from pathlib import Path
from typing import TYPE_CHECKING, Any, NamedTuple

from PySide6.QtCore import QSize

from constants import EVENT_WIN_SIZE, EVENT_WIN_POS, CLR_NODE_START, DEFAULT, GENERAL, PRESET
from gui.editor.keyboard import KeyboardView
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from gui.editor.node import Node, ProgramNode
from midi.midi_const import DEFAULT_VERSION_NAME, MAX_CHANNEL, DEFAULT_SF2, DEFAULT_BANK, DEFAULT_PATCH
from midi.synth import FS
from model.composition import Composition
from model.note import Preset, DEFAULT_PRESET, ChannelValue
from model.project import Project
from model.track import TrackVersion, Track

if TYPE_CHECKING:
    from gui.main_frame import MainFrame
from typing import Optional

from PySide6.QtGui import Qt, QIcon, QColor, QPalette
from PySide6.QtWidgets import QWidget, QDialog, QBoxLayout, QTabWidget, QSplitter, QListWidget, \
    QListWidgetItem, QToolButton, QLineEdit, QCheckBox, QColorDialog, QFormLayout, QDialogButtonBox, QAbstractButton

from gui.widgets import Box, ChannelBox
import resources


class GenericConfigMode(Flag):
    new = auto()
    edit = auto()


class GenericConfig(NamedTuple):
    mf: MainFrame
    mode: Optional[GenericConfigMode] = None
    composition: Optional[Composition] = None
    track: Optional[Track] = None
    track_version: Optional[TrackVersion] = None
    node: Optional[Node] = None


class GenericConfigDlg(QDialog):
    def __init__(self, parent):
        super().__init__(parent=parent)
        self.config: Optional[GenericConfig] = None
        self.setSizeGripEnabled(True)
        self.tab_box = QTabWidget()
        self.general = GeneralTab(parent=self)
        self.preset = PresetTab(parent=self)
        self.tab_map = {
            GENERAL: self.tab_box.addTab(self.general, GENERAL),
            PRESET: self.tab_box.addTab(self.preset, PRESET)
        }
        self.buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal)
        self.main_box = Box(direction=QBoxLayout.TopToBottom)
        self.main_box.setContentsMargins(10, 10, 10, 10)
        self.main_box.setSpacing(10)
        self.main_box.addWidget(self.tab_box)
        self.main_box.addWidget(self.buttons)
        self.setLayout(self.main_box)

        self.buttons.clicked.connect(self.button_clicked)

    def button_clicked(self, button: QAbstractButton):
        if self.buttons.buttonRole(button) == QDialogButtonBox.AcceptRole:
            if self.main_layout.is_valid():
                self.accept()
        else:
            self.reject()

    def load_config(self, config: GenericConfig):
        self.config = config
        self.setWindowTitle(f'Node settings' if config.node else f'General settings')
        self.load_window_geometry(config=config)
        if config.node:
            self.tab_box.setTabEnabled(self.tab_map[GENERAL], False)
        else:
            self.tab_box.setTabEnabled(self.tab_map[GENERAL], True)
            self.general.load_config(config=config)
        self.preset.load_config(config=config)

    def load_window_geometry(self, config: GenericConfig):
        if not self.isVisible():
            size = config.mf.config.value(EVENT_WIN_SIZE, QSize(500, 400))
            pos = config.mf.config.value(EVENT_WIN_POS, None)
            if pos:
                self.setGeometry(pos.x(), pos.y(), size.width(), size.height())
            else:
                self.resize(size)

    def closeEvent(self, e):
        self.config.mf.config.setValue(EVENT_WIN_SIZE, self.size())
        self.config.mf.config.setValue(EVENT_WIN_POS, self.pos())


class PresetTab(QWidget):
    def __init__(self, parent):
        super().__init__(parent=parent)
        self.config: Optional[GenericConfig] = None
        self.sf_list = QListWidget()
        self.sf_list.resize(280, self.sf_list.height())
        self.bank_list = QListWidget()
        self.bank_list.resize(50, self.bank_list.height())
        self.prog_list = QListWidget()
        self.keyboard = KeyboardView(synth=None, channel=self.channel, callback=None)
        self.splitter_right = QSplitter(Qt.Horizontal)
        self.splitter_right.addWidget(self.bank_list)
        self.splitter_right.addWidget(self.prog_list)
        self.splitter_right.addWidget(self.keyboard)
        self.splitter = QSplitter(Qt.Horizontal)
        self.splitter.addWidget(self.sf_list)
        self.splitter.addWidget(self.splitter_right)
        self.main_box = Box(direction=QBoxLayout.LeftToRight)
        self.main_box.addWidget(self.splitter)
        self.setLayout(self.main_box)

        self.sf_list.itemSelectionChanged.connect(self.on_sf_change)
        self.bank_list.itemSelectionChanged.connect(self.on_bank_change)
        self.prog_list.itemActivated.connect(self.on_prog_activated)
        self.prog_list.currentItemChanged.connect(self.on_prog_selected)

    @property
    def channel(self) -> ChannelValue:
        return MAX_CHANNEL - 1

    def load_config(self, config: GenericConfig):
        self.config = config
        self.keyboard.set_synth_and_channel(synth=config.mf.synth,
                                            channel=self.channel)
        self.populate_fonts()

    def set_initial_preset(self) -> None:
        if self.config.node and type(self.config.node) == ProgramNode:
            pass
        elif self.config.track_version:
            init_sf_name = self.config.track_version.sf_name
            init_bank = self.config.track_version.bank
            init_patch = self.config.track_version.patch
        elif self.config.track:
            init_sf_name = self.config.track.default_sf
            init_bank = self.config.track.default_bank
            init_patch = self.config.track.default_patch
        else:
            init_sf_name = DEFAULT_SF2
            init_bank = DEFAULT_BANK
            init_patch = DEFAULT_PATCH
        self.select_data(lst=self.sf_list, data=init_sf_name)
        items = self.bank_list.findItems(str(init_bank), Qt.MatchExactly)
        if items:
            bank = self.bank_list.indexFromItem(items[0]).row()
        else:
            bank = 0
        if self.bank_list.count():
            self.bank_list.setCurrentRow(bank)
            self.bank_list.scrollToItem(self.bank_list.currentItem())
            if self.prog_list.count():
                self.select_data(self.prog_list, Preset(sfid=self.config.mf.synth.sf_map[init_sf_name],
                                                        bank=bank,
                                                        patch=init_patch))

    @staticmethod
    def select_data(lst: QListWidget, data: Any) -> None:
        for row in range(lst.count()):
            item = lst.item(row)
            if item.data(Qt.UserRole) == data:
                lst.setCurrentItem(item)
                lst.scrollToItem(item)
                return
        raise ValueError(f'Cannot find data {data} in list {lst}')

    def on_sf_change(self):
        if self.sf_list.currentItem():
            last_bank = self.bank_list.currentItem().text() if self.bank_list.currentItem() else None
            sf_name = self.sf_list.currentItem().data(Qt.UserRole)
            sfid = self.config.mf.synth.sf_map[sf_name]
            print('on_sf_change', sf_name, sfid)
            self.populate_banks(sfid=sfid)
            if self.bank_list.count() > 0:
                self.bank_list.setCurrentRow(0)
                if last_bank is not None:
                    if items := self.bank_list.findItems(last_bank, Qt.MatchExactly):
                        item, *rest = items
                        self.bank_list.setCurrentItem(item)
            self.bank_list.scrollToItem(self.bank_list.currentItem())

    def on_bank_change(self):
        if self.sf_list.currentItem() and self.bank_list.currentItem():
            sf_name = self.sf_list.currentItem().data(Qt.UserRole)
            sfid = self.config.mf.synth.sf_map[sf_name]
            bank = self.bank_list.currentItem().text()
            print('on_bank_change', sf_name, sfid, bank)
            self.populate_programs(sfid=sfid,
                                   bank=int(bank))
            if self.prog_list.count() > 0:
                self.prog_list.setCurrentRow(0)
                self.prog_list.scrollToItem(self.prog_list.currentItem())

    def on_prog_activated(self, item: QListWidgetItem):
        preset = item.data(Qt.UserRole)
        # self.config.track_version

    def on_prog_selected(self, current: QListWidgetItem, previous: QListWidgetItem):
        if current:
            preset = current.data(Qt.UserRole)
            self.config.mf.synth.preset_change(channel=self.channel, preset=preset)

    def populate_fonts(self):
        self.sf_list.itemSelectionChanged.disconnect()
        self.sf_list.clear()
        self.bank_list.clear()
        self.prog_list.clear()
        for sf_path in self.config.mf.synth.sf_map.keys():
            item = QListWidgetItem(Path(sf_path).name, self.sf_list)
            item.setIcon(QIcon(":/icons/sf.png"))
            item.setData(Qt.UserRole, sf_path)
            self.sf_list.addItem(item)
        self.sf_list.itemSelectionChanged.connect(self.on_sf_change)
        self.set_initial_preset()

    def populate_banks(self, sfid: int):
        curr_sf_name = self.sf_list.currentItem().data(Qt.UserRole)
        curr_sfid = self.config.mf.synth.sf_map[curr_sf_name]
        self.bank_list.itemSelectionChanged.disconnect()
        if sfid == curr_sfid:
            self.bank_list.clear()
            for bank in self.config.mf.synth.preset_map[sfid].keys():
                item = QListWidgetItem(str(bank), self.bank_list)
                item.setIcon(QIcon(":/icons/bank.png"))
                self.bank_list.addItem(item)
        self.bank_list.itemSelectionChanged.connect(self.on_bank_change)

    def populate_programs(self, sfid: int, bank: int):
        self.prog_list.currentItemChanged.disconnect()
        if bank == int(self.bank_list.currentItem().text()):
            self.prog_list.clear()
            print('populate_programs', sfid, bank)
            for patch, preset in self.config.mf.synth.preset_map[sfid][bank].items():
                item = QListWidgetItem(f"{str(patch)}: {preset}")
                item.setIcon(QIcon(":/icons/preset.png"))
                item.setData(Qt.UserRole, Preset(sfid=sfid, bank=bank, patch=patch))
                self.prog_list.addItem(item)
        self.prog_list.currentItemChanged.connect(self.on_prog_selected)


class GeneralTab(QWidget):
    def __init__(self, parent):
        super().__init__(parent=parent)
        self.config: Optional[GenericConfig] = None
        self.form = QFormLayout()
        self.form.setContentsMargins(10, 10, 10, 10)
        self.form.setSpacing(5)
        self.project_name = QLineEdit()
        self.composition_name = QLineEdit()
        self.track_name = QLineEdit()
        self.track_color = QToolButton()
        self.version_name = QLineEdit()
        self.version_channel = ChannelBox()
        self.enable_in_loops = QCheckBox('Enable track in loops')

        # Form
        self.form.addRow('Project name', self.project_name)
        self.form.addRow('Composition name', self.composition_name)
        self.form.addRow('Track name', self.track_name)
        self.form.addRow('Track color', self.track_color)
        self.form.addRow('Version name', self.version_name)
        self.form.addRow('Version channel', self.version_channel)
        self.form.addRow('', self.enable_in_loops)

        self.setLayout(self.form)
        self.track_color.clicked.connect(self.get_track_color)

    def load_config(self, config: GenericConfig):
        self.config = config
        self.project_name.setText(config.mf.project.name)
        self.composition_name.setText(config.composition.name)
        self.track_name.setText(config.track.name if config.track else '')
        self.show_track_color(color=QColor.fromRgba(config.track.default_color) if config.track else CLR_NODE_START)
        self.version_name.setText(config.track_version.version_name if config.track_version else DEFAULT_VERSION_NAME)
        self.version_channel.setCurrentIndex(config.track_version.channel
                                             if config.track_version
                                             else config.composition.get_next_free_channel())
        self.enable_in_loops.setChecked(True if not config.track else False)

    def get_track_color(self):
        color = QColorDialog.getColor(self.track_color.default
                                      if hasattr(self.track_color, DEFAULT) else CLR_NODE_START)
        if color:
            self.track_color.default = color.rgba()
            self.show_track_color(color=color)

    def show_track_color(self, color: QColor):
        self.track_color.setAutoFillBackground(True)
        pal = self.track_color.palette()
        pal.setColor(QPalette.Button, color)
        self.track_color.setPalette(pal)

    def is_track_name_valid(self) -> bool:
        valid = self.get_track_name() != ''
        if not valid:
            self.config.mf.show_message_box('Track name is empty')
            return valid
        valid = not self.config.composition.track_name_exists(track_name=self.get_track_name())
        if not valid:
            self.config.mf.show_message_box(f'Track name {self.get_track_name()} exists in composition')
        return valid

    def is_track_version_name_valid(self) -> bool:
        valid = self.get_version_name() != ''
        if not valid:
            self.config.mf.show_message_box('Version name is empty')
        return valid

    def is_valid(self) -> bool:
        return self.is_track_name_valid() and self.is_track_version_name_valid()

    def get_track_name(self) -> str:
        return self.track_name.text().strip()

    def get_version_name(self) -> str:
        return self.version_name.text().strip()

    def get_default_channel(self):
        return self.config.composition.get_first_track_version().channel

    def get_default_num_of_bars(self):
        return self.config.composition.get_first_track_version().num_of_bars
